﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    public abstract class Goods
    {
        protected string _name;
        public string Name              // наименование товара
        {
            get { return _name; }
            set { _name = value;}
        }                      
        protected double _purchasePrise;      // цена закупки
        public double PurchasePrise
        {
            get { return _purchasePrise; }
            set { _purchasePrise = value; }
        }
        protected double _koef;          // процент накрутки
        public double Koef
        {
            get { return _koef; }
            set { _koef = value; }
        }
        protected double _prise;         // цена продажи
        public double Prise
        {
            get { return _prise; }
            set { _prise = value; }
        }
        protected int _balance;       // остаток на складе
        public int Balance
        {
            get { return _balance; }
            set { _balance = value; }
        }
        protected DateTime _start;         // дата получения
        public DateTime Start
        {
            get { return _start; }
            set { _start = value; }
        }

        protected double _contractNumber;             // номер договора
        public double ContractNumber
        {
            get { return _contractNumber; }
            set { _contractNumber = value; }
        }
        protected double _invoiceNumber;             // номер накладной
        public double InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { _invoiceNumber = value; }
        }

        protected bool _isDefect = false;        // по умолчанию товар не бракован
        public bool IsDefect
        {
            get { return _isDefect; }
            set { _isDefect = value; }
        }

        protected int _incoming;           // все поступления товара
        public int Incoming
        {
            get { return _incoming; }
            set { _incoming = value; }
        }

        protected int _selling;           // всего реализовано товара
        public int Selling
        {
            get { return _selling; }
            set { _selling = value; }
        }

        protected int _writeOff;           // всего списано товара
        public int WriteOff
        {
            get { return _writeOff; }
            set { _writeOff = value; }
        }

        protected int _returned;           // всего возвращено товара
        public int Returned
        {
            get { return _returned; }
            set { _returned = value; }
        }


        public override string ToString()
        {
            return string.Format("Name of goods: {0}\t\tbalance: {1}\t\tprise: {2}", _name, _balance, _prise);
        }

        public Goods() { }

        public Goods(string name, int balance, double purchasePrise)
        {
            _name = name;
            _balance = balance;
            _purchasePrise = purchasePrise;
            _incoming = balance;            
        }       
    }
}

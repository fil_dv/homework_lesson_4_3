﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class GoodsExpiredException : MyException
    {
         public GoodsExpiredException()
            : base()  { }

        public GoodsExpiredException(string message)
             : base() { message = "Goods is expired"; }

        public GoodsExpiredException(string message, Exception innerException)
            : base(message, innerException) { }

        public GoodsExpiredException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}

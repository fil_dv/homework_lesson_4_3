﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class Alcohol : Foodstuffs, IAlcoholTobacco
    {
        public Alcohol(string name, int balance, double purchasePrise)
            : base(name, balance, purchasePrise)  
        {
           
        }
        
        public double LicenseNumber { get; set; }
        public DateTime TermLicense { get; set; }        
    }
}

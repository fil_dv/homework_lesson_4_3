﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class AirFreshener : HouseholdChemicals, IFlammable
    {
        public AirFreshener(string name, int balance, double purchasePrise)
            : base(name, balance, purchasePrise)
        {
           
        }
        
        public int MaxTemperature {get; set;}
    }
}

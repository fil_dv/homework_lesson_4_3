﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class Eggs : Foodstuffs, IPerishableFood, IFragile
    {
        public Eggs(string name, int balance, double purchasePrise)
            : base(name, balance, purchasePrise)  
        {
            
        }

       

        public int NumberOfStacks {get; set;}

        public DateTime EndDate {get; set;}        

        public DateTime SoonDate {get; set;}
        
    }
}

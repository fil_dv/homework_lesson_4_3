﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class Monitors : HouseholdAppliances, IFragile
    {
        public Monitors(string name, int balance, double purchasePrise, int warranty)
            : base(name, balance, purchasePrise, warranty)  
        {
            
        }
        
        public int NumberOfStacks {get; set;}
    }
}

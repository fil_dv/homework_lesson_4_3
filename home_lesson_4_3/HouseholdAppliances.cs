﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    abstract class HouseholdAppliances : ManufacturedGoods
    {
        protected int _warranty;
        public int Warranty
        {
            get { return _warranty; }
            set { _warranty = value; }
        }

        public HouseholdAppliances(string name, int balance, double purchasePrise, int warranty)
            : base(name, balance, purchasePrise)  
        {
            _warranty = warranty;
        }

    }
}

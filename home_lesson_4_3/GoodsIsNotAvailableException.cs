﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class GoodsIsNotAvailableException : MyException
    {
         public GoodsIsNotAvailableException()
            : base()  { }

        public GoodsIsNotAvailableException(string message)
             : base() { message = "Goods is not available"; }

        public GoodsIsNotAvailableException(string message, Exception innerException)
            : base(message, innerException) { }

        public GoodsIsNotAvailableException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}

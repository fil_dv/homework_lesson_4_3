﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class Meat : Foodstuffs, IPerishableFood
    {
        public Meat(string name, int balance, double purchasePrise)
            : base(name, balance, purchasePrise)
        {
            
        }
        
        public DateTime EndDate { get; set; }        
        public DateTime SoonDate { get; set; }        
    }
}

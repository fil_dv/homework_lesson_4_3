﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    abstract class ManufacturedGoods : Goods
    {
        public ManufacturedGoods(string name, int balance, double purchasePrise)
            : base(name, balance, purchasePrise)
        {
            _koef = 1.5;
            _prise = purchasePrise * _koef;  
        }

    }
}

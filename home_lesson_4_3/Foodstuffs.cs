﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    abstract class Foodstuffs : Goods
    {
        public Foodstuffs(string name, int balance, double purchasePrise)
            : base(name, balance, purchasePrise) 
        {
            _koef = 1.3;
            _prise = purchasePrise * _koef;
        }
        
    }
}

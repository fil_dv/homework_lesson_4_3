﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    class DefectiveGoodsException : MyException
    {
         public DefectiveGoodsException()
            : base()  { }

        public DefectiveGoodsException(string message)
             : base() { message = "Goods is defectiv"; }

        public DefectiveGoodsException(string message, Exception innerException)
            : base(message, innerException) { }

        public DefectiveGoodsException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}

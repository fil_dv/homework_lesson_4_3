﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    public class Accounting
    {
        public Goods[] _arr;
        double _spending = 0;   //затраты на преобретение товаров
        double _proceeds = 0;   // "выручка"
        double _profit = 0;     // "прибыль"
        double _loss = 0;       // "убыток"

        public Accounting()
        {
            _arr = new Goods[0]; 
        }

        public Accounting(GoodsEnum e1, Goods g1, GoodsEnum e2, Goods g2, GoodsEnum e3, Goods g3, GoodsEnum e4, Goods g4, GoodsEnum e5, Goods g5)
        {
            _arr = new Goods[5];
            _arr[(int)e1] = g1;
            _arr[(int)e2] = g2;
            _arr[(int)e3] = g3;
            _arr[(int)e4] = g4;
            _arr[(int)e5] = g5;
            for (int i = 0; i < _arr.Length; ++i)
            {
                _spending += _arr[i].PurchasePrise * _arr[i].Incoming;
            }
        }

        public void AddNewGoods(GoodsEnum e, Goods g)      // добавление нового вида товара в массив
        {
            Array.Resize(ref _arr, _arr.Length +1);
            e = (GoodsEnum)_arr.Length - 1;
            _arr[(int)e] = g;
            _spending += _arr[(int)e].PurchasePrise * _arr[(int)e].Incoming;
        }

        public void AddToBallans(GoodsEnum e, int num)      // добавить на баланс
        {
            _arr[(int)e].Balance += num;
            _arr[(int)e].Incoming += num;
            _spending += _arr[(int)e].PurchasePrise * num;
        }

        public void Print()
        {
            for (int i = 0; i < _arr.Length; ++i)
            {
                Console.WriteLine(_arr[i].ToString());
            }
            Console.WriteLine("");
        }

        public void PrintByPosition(GoodsEnum g)                    // распечатка по позиции
        {
            Console.WriteLine(_arr[(int)g].ToString() + "\n");
        }

        public void PrintCurrentResult()
        {
            double circulation = 0;
            for (int i = 0; i < _arr.Length; ++i)
            {
                circulation += _arr[i].PurchasePrise * _arr[i].Balance;
            }

            Console.WriteLine("Total purchase costs: \t {0} hrn. \nTotal proceeds: \t {1} hrn. \nMoney in circulation: \t {2} hrn. \nlosses: \t\t {3} hrn. \nProfit on sales: \t {4} hrn.\n", _spending, _proceeds, circulation, _loss, _profit);
        }

        public void RePrise(GoodsEnum g, double d)                  // изменить коэффициент наценки и переоценить товар
        {
            _arr[(int)g].Koef = d;
            _arr[(int)g].Prise = _arr[(int)g].PurchasePrise * d;
        }

        public void RePurchasePrise(GoodsEnum g, double d)        // изменить цену закупки товара
        {
            _arr[(int)g].Koef = d;
            _arr[(int)g].Prise = _arr[(int)g].PurchasePrise * d;
        }

        public void Sale(GoodsEnum g, int num)       // продажа товара
        {
            if (num > _arr[(int)g].Balance) throw new GoodsIsNotAvailableException();
            else
            {
                _arr[(int)g].Balance -= num;                                 // списание с остатка
                _arr[(int)g].Selling += num;                                 // приращение продаж
                _proceeds += _arr[(int)g].Prise * num;                       // приращение выручки
                _profit += _proceeds - _arr[(int)g].PurchasePrise * num;     // приращение прибыли

            }
        }

        public void WriteOff(GoodsEnum g, int num)      // списание товара
        {
            if (num > _arr[(int)g].Balance) throw new GoodsIsNotAvailableException();
            else
            {
                _arr[(int)g].Balance -= num;                // списание с остатка
                _arr[(int)g].WriteOff += num;               // приращение учета списаний
                _loss += _arr[(int)g].PurchasePrise * num;  // приращение убытков
            }            
        }

        public void Return(GoodsEnum g, int num)      // возврат товара
        {
            if (num > _arr[(int)g].Balance) throw new GoodsIsNotAvailableException();
            else
            {
                _arr[(int)g].Balance -= num;                    // списание с остатка
                _arr[(int)g].Returned += num;                   // приращение учета возвратов
                _spending -= _arr[(int)g].PurchasePrise * num;  // списание затрат
            }
        }




    }
}

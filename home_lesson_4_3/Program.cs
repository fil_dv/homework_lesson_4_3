﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    public enum GoodsEnum { eggs, meat, airFreshers, laptops, monitors }; 

    class Program
    {
        static void Main(string[] args)
        {             
            Eggs e = new Eggs("eggs", 30, 10);
            Meat m = new Meat("chicken", 10, 30);            
            AirFreshener a = new AirFreshener("Rose", 5, 10);            
            Monitors mon = new Monitors("LG", 3, 1000, 12);            
            Laptops lp = new Laptops("HP", 3, 3000, 24);

            GoodsEnum egg = GoodsEnum.eggs;
            GoodsEnum me = GoodsEnum.meat;
            GoodsEnum air = GoodsEnum.airFreshers;
            GoodsEnum lap = GoodsEnum.laptops;
            GoodsEnum monik = GoodsEnum.monitors;
            /*
            Accounting acc = new Accounting(egg, e, me, m, air, a, lap, lp, monik, mon);
            acc.Print();
            acc.AddToBallans(egg, 20);
            acc.RePrise(egg, 2);
            acc.PrintByPosition(egg);
            acc.Print();
            acc.Sale(monik, 1);
            acc.PrintCurrentResult();
            */
            Accounting acc1 = new Accounting();
            acc1.AddNewGoods(egg, e);
            acc1.Sale(egg, 20);
            acc1.Print();
            acc1.PrintCurrentResult();

        }
    }
}

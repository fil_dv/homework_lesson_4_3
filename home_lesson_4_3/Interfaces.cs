﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace home_lesson_4_3
{
    interface IPerishableFood   // скоропортящиеся
    {        
        DateTime EndDate { get; set; }      // окончание срока годности
        DateTime SoonDate { get; set; }     // время установления скидки
    }

    interface IAlcoholTobacco   // акцизные
    {
        double LicenseNumber { get; set; }      // номер лизенции
        DateTime TermLicense { get; set; }      // срок действия лицензии
    }

    interface IFlammable    // легко воспламеняющиеся
    {
        int MaxTemperature { get; set; }    // температура хранения
    }

    interface IFragile     // хрупкие
    {
        int NumberOfStacks { get; set; }    // максимальное кол-во штабелей при хранении
    }
}
